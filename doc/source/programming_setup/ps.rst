.. _setup_workshop_repository:

Setup
#####

#. It is assumed that you already followed the
   `installation instruction <https://enmap-box.readthedocs.io/en/latest/dev_section/dev_installation_slim.html>`_.

#. Download the
   `EnMAP-Box Workshop 2019 Repository <https://bitbucket.org/hu-geomatics/enmap-box-workshop2019/get/develop.zip>`_.

#. Unzip the content to a location of your choice (e.g. ``C:\source\enmap-box-workshop2019``)

#. Start **PyCharm**, open the **Settings** dialog: *File > Settings...*

    - select **Project: EnMAP-Box** and **Project Structur**

    - click the **+ Add Content Root** button and enter the **EnMAP-Box Workshop 2019 Repository** folder

    .. image:: content_root.png

    - confirm with **OK** and **OK**

#. In the **Project** view (show with [Alt+1]), right click on **enmap-box-workshop2019/programming_tutorial1** and *Mark Directory as > Sources Root*
