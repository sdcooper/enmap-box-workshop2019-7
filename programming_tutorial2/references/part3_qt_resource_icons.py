import os.path, sys
from enmapbox.testing import initQgisApplication
app = initQgisApplication()
from enmapbox import DIR_REPO


# ensure that the enmapbox/make folder is added to the PYTHONPATH
dirMakeScripts = os.path.join(DIR_REPO, 'make')
if dirMakeScripts not in sys.path:
    sys.path.append(dirMakeScripts)

import iconselect
iconselect.run()
