# -*- coding: utf-8 -*-

"""
***************************************************************************
    examples/minimumexample/tests.py

    Unit tests to check the minimum example
    ---------------------
    Date                 : January 2019
    Copyright            : (C) 2019 by Benjamin Jakimow
    Email                : benjamin.jakimow@geo.hu-berlin.de
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""


from unittest import TestCase

from enmapbox.testing import initQgisApplication
from myworkshopapp.workshopapp import *

# initialize the QGIS API and emulate a QGIS desktop instance
APP = initQgisApplication()

# set on True to show widgets and wait until a user closes them.
SHOW_GUI = True

class WorkshopEnMAPBoxApplicationTests(TestCase):


    def test_gui_witouth_enmapbox(self):
        """
        Test your GUI components, without any EnMAP-Box
        """
        from enmapboxworkshopui import EnMAPBoxWorkshopUI
        w = EnMAPBoxWorkshopUI()
        w.show()

        if SHOW_GUI:
            APP.exec_()


    def test_with_enmapbox(self):
        """
        Check if your application can be added to the EnMAP-Box
        """
        from enmapbox import EnMAPBox
        enmapBox = EnMAPBox(None)
        self.assertIsInstance(enmapBox, EnMAPBox)

        myApp = MyEnMAPBoxWorkshopApplication(enmapBox)
        self.assertIsInstance(myApp, EnMAPBoxApplication)
        enmapBox.addApplication(myApp)

        if SHOW_GUI:
            APP.exec_()



if __name__ == "__main__":
    import unittest
    SHOW_GUI = False
    unittest.main()

