from qgis.core import QgsVectorLayer
from enmapbox.testing import initQgisApplication
app = initQgisApplication()

from enmapbox.gui import ClassificationSchemeWidget, ClassificationSchemeComboBox, ClassificationScheme, ClassInfo
from enmapboxtestdata import landcover_polygons

layer = QgsVectorLayer(landcover_polygons)
classificationScheme = ClassificationScheme.fromMapLayer(layer)

classSchemeWidget = ClassificationSchemeWidget(classificationScheme=classificationScheme)
classSchemeWidget.setWindowTitle('Classification Scheme Widget')
classSchemeWidget.show()

classSchemeComboBox = ClassificationSchemeComboBox(classification=classificationScheme)
classSchemeComboBox.setWindowTitle('Classification Scheme ComboBox')
classSchemeComboBox.show()

for classInfo in classificationScheme:
    assert isinstance(classInfo, ClassInfo)
    print('Label: Name:{} Color: {}'.format(
        classInfo.label(),
        classInfo.name(),
        classInfo.color().getRgb()))

app.exec_()
